# Extended Utitility Library

A collection of useful functions to handle strings, number or tables.

Usage:

    package.path = package.path .. \";data/scripts/lib/?.lua
    include ("extutils"))

This collection is intended to grow over time. If you have a great idea what I should add to it, feel free to contact me. Maybe I will develop what you are thinking about, or include the function you gave me.

Credits:

`spairs`: Michal Kottman [stackoverflow.com]  
`serialize`: Fabien Fleutot [github.com]

