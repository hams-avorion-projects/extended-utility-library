# Changelog
### 1.0.0
* Updated mod for Avorion 2.*

### 0.1.2
* Updated mod for Avorion 1.*

### 0.1.1
* Update mod for Avorion 0.23 or higher

### 0.1.0
* Create project

